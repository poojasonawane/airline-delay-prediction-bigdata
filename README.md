								Airline Delay Prediction
Deep Divers
Nikhil Junneti		
Pooja Sonawane	
Sandeep Sunia		
Umesh Jain

1.	Introduction
Flight delay is a serious problem in the United States. Each year, approximately 0.8 billion passengers use airline services which are provided by around 9 million commercial flights.[10] Of these nearly, 25% of the flights are delayed.[1] There are several reasons which may cause a flight to be delayed, some of which include weather, security, previous aircraft arriving late. These delays cause heavy losses to airlines, passengers and society. Through this project, given the details of a flight, we predicted whether it will be delayed or not. Knowing this prediction might help the passenger to plan and modify their journeys accordingly.

2.	Background
	There are several models which are implemented in past to predict the delays in flight. But there are very few models which included streaming of flight data. In our project, we have trained and tested our model on the past data. We also test the model on streamed data and later use this data to retrain our model, so as to improve its accuracy. We also provide a comparative analysis of results obtained from several models like, DecisionTree (binary and multiclass), RandomForest, LogisticRegression.
	Considering the large size of our dataset, we have used Stochastic Gradient Descent approach for all of the above models. We used Spark and SparkStreaming for parallel processing and streaming the data respectively.
 
3.	Data
	We are using data provided by Bureau of Transportation Statistics. The dataset contains flight information of 29 domestic airlines, 305 airports and 30 features. We collected data from 2006 to 2016, which contributed to a total of 6GB. Each month has data of more than 500,000 flights. Some of the features include date of flight, flight number, tail number, origin, destination, scheduled departure and arrival times, actual departure and arrival times, total time elapsed, distance between origin and destination airports, departure and arrival delays, possible causes of delay and delay in minutes, information about flight diversions, cancellations, taxi in, taxi out times, total air time, etc. We used this dataset for the first part of model building(which is explained later).
In the second part, we increased the total number of features to 678 which contributed to a data size of approximately 1.2 GB for each month. We increased the number of features by changing useful categorical variables like year, month of year, day of month, day of week, unique carrier codes, origin and destination airports to numerical variables. For training and testing of this model, we considered data of September 2016.

4.	Methods
4.1. Preprocessing
For preprocessing the data, we  performed following tasks:
-	Eliminating flights which were cancelled.
-	Changed the format of columns such as hour of day from 1500 to 15:00.
-	We added a new feature to determine the proximity of flight departure day with a nearest holiday.

	4.2 Data Analysis
-	In preliminary data analysis, we observed that average delay times across all airports are different in different months with peaks in February and December which can be expected because of long holidays in this season (fig. a). 
-	We also observed effect of hour of the day on delay times, with most delay around 8PM in the evening. Flights towards the end of the day experience more delays than in the early part of the day.
4.3 Feature Selection
-	We eliminated features which had less correlation with departure delay, such as tail number, taxi in, taxi out.
-	We had a constraint to consider only those variables that could be acquired from real time data. Our data contains many features which includes reason for delay like carrier delay, national aviation delay, system delay, late aircraft delay, arrival delay etc. These features correlated very well with departure delay but we cannot consider all of these since we don’t know these values before the flight departs. So, we eliminated such features.
-	So, this leaves us with features like year, month, day of month, day of week, scheduled departure time, distance between origin and destination, unique carrier code.

4.4 Baseline model
-	In all our models, we considered flights with delay greater than 15 minutes as delayed, whereas, the ones with less than 15 minutes as not delayed. So our problem is binary classification problem which predicts whether a given flight is delayed or not.
-	We used Zero rule algorithm as a baseline model. This algorithm predicts the class which appears most frequently in the training data(‘Not Delayed’ class in our case). This model gave us an accuracy of 0.75.

4.4 Advanced models
	We implemented below given models in two parts. 
-	In the first part, our models included features like Day of Month, Day of Week, Distance, Hour of Day, Holiday Proximity. In this, we had separate model for each airport. 
-	In second part, added additional features by doing one hot-encoding of the categorical features like origin, destination, unique_carrier (airline identification code), day of month and day of week. This model included all the airports.

As per the scikit-learn algorithm cheat sheet we tried the below models:
Logistic Regression: We tested our model using Logistic Regression Classifier with SGD. Since the data size is very large, we used this linear classifier with SGD.
Decision Tree Classifier: We also tested upon Decision Tree classifier with two classes.
Random Forest Classifier: We tried random forest model for two classes from ensemble methods. 
Multi Class Decision Tree: We created 5 classes for delay time ranges. Class0: delay time<=0, Class1: 0<delay time<=10, Class 2: 10<delay time<=20, Class 3 : 20<delay time<=30, Class4: delay time>30min.
We also tried LinearSVC classifier but we are not documenting it since its performance was not good compared to other methods discussed.

7.	Conclusion
We were able to observe some features from the data:
-	Actual Flight Lengths  are less than the Scheduled Flight Length. This means the airline carriers are taking into account possible delays and inflating the runtime, to avoid negative publicity.
-	Actual Flight Length is measured gate to gate, this figure also includes taxiing to the runway, waiting in line to takeoff, and taxiing back to the gate after landing.  This could explain why airlines have different scheduled flight times.  For instance, maybe one airline’s gate is farther away from the runway than another’s.
-	Federal Holidays does not predict flight delays. Delays are more dependent upon airport and Airline.
-	Encoding categorical variable to numerical gave very good results indicating predicting power of those variables.
-	We could also have enriched this model with weather data but collecting and updating dataset with weather data for all airports is difficult and we do not have enough resources. 


8.	References
[1] http://www.rita.dot.gov/bts/press_releases/bts018_16 
[2] http://www.transtats.bts.gov/ 
[3] http://spark.apache.org/docs/latest/ml-features.html 
[4] http://spark.apache.org/docs/latest/ml-classification-regression.html 
[5] https://spark.apache.org/docs/1.2.0/mllib-guide.html 
[6] http://spark.apache.org/docs/latest/streaming-programming-guide.html 
[7]http://machinelearningmastery.com/classification-accuracy-is-not-enough-more-performance-measures-you-can-use/
[8]https://databricks.com/blog/2016/05/31/apache-spark-2-0-preview-machine-learning-model-persistence.html
[9]http://hortonworks.com/blog/data-science-apacheh-hadoop-predicting-airline-delays/
[10]https://www.crowdanalytix.com/contests/predict-airline-delay-for-domestic-and-international-flights-in-us
[11]http://www.transtats.bts.gov/ot_delay/ot_delaycause1.asp 


