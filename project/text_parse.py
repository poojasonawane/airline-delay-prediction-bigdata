from pyspark.sql import functions as F 
from pyspark.sql.types import StringType
from pyspark import SQLContext
from pyspark import SparkContext
from pyspark.sql import SparkSession
from pyspark.ml.feature import OneHotEncoder, StringIndexer
from pyspark.mllib.linalg import Vectors
from pyspark.ml.feature import VectorAssembler
from IPython.display import display
#from ipywidgets import interact

def toCSVLine(data):
  return ','.join(str(d) for d in data)
  
spark = SparkSession\
            .builder\
            .appName("Appname")\
            .getOrCreate()
sc = spark.sparkContext

sqlContext = SQLContext(sc)
df_model = (
    sqlContext.read
    .format('com.databricks.spark.csv')
    .options(inferSchema = True)
    .load("../../testinput.csv")
)



stringIndexer1 = StringIndexer(inputCol='_c0', outputCol='originIndex')


model_stringIndexer = stringIndexer1.fit(df_model)
indexedOrigin = model_stringIndexer.transform(df_model)
encoder1 = OneHotEncoder(dropLast=True, inputCol='originIndex', outputCol='ORIGINVEC')
df_model = encoder1.transform(indexedOrigin)

assembler = VectorAssembler(
    inputCols = ['_c2','_c3','_c4','_c7','_c8','ORIGINVEC'],
    outputCol = 'FEATURES')
output = assembler.transform(df_model)

airlineRDD=output.rdd.map(lambda row: LabeledPoint([0,1][row['DEP_DELAYED']],row['FEATURES'])).map(toCSVLine).saveAsTextFile("categorset_test.csv")

