import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark.rdd._
import scala.collection.JavaConverters._
import scala.collection.mutable.ListBuffer
import java.io._
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.mllib.feature.StandardScaler
import java.time.LocalDate 
import java.time.format.DateTimeFormatter
import org.apache.spark.mllib.classification.LogisticRegressionWithSGD
import org.apache.spark.mllib.tree.DecisionTree
import org.apache.spark.mllib.regression.LinearRegressionModel
import org.apache.spark.mllib.regression.LinearRegressionWithSGD
import org.apache.spark.mllib.util.MLUtils
import org.apache.spark.mllib.evaluation.BinaryClassificationMetrics
import org.apache.spark.mllib.tree.RandomForest
import org.apache.spark.mllib.tree.model.RandomForestModel
import org.apache.spark.mllib.evaluation.MulticlassMetrics
import org.apache.spark.mllib.classification.SVMWithSGD

object PredictFromSavedModel_WOstreaming{

def main(args: Array[String]) {
	val conf = new SparkConf().setAppName("Flight Delay Detection")
	val sc = new SparkContext(conf)
	val data_test = sc.textFile("hdfs://categorset_test.csv").filter(_.nonEmpty)
	val test = data_test.map(PredictFromSavedModel_WOstreaming.preProcess)//map data to features.

//////////////////// Prepare test set
	val parsedTestData = test.map(parseData)
	val scaler = new StandardScaler(withMean = true, withStd = true).fit(parsedTestData.map(x => x.features))
	val scaledTrainData = parsedTestData.map(x => LabeledPoint(x.label, scaler.transform(Vectors.dense(x.features.toArray))))
	
///////////////////Predict	
	val model_dt = DecisionTreeModel.load(sc, "hdfs://target/tmp/myDecisionTreeClassificationModel")	
	val preds_dt = scaledTestData.map { point =>
		val pred = model_dt.predict(point.features)
		pred		
	}
	val outputs = Preds_dt.collect().saveAsTextFile("hdfs://output.txt")
}
////preprocess data.
def preProcess(data: String): Array[Double]={
	val input = data.split(",").dropRight(1);
	val formattedInput = input.map(value => if (!value.isEmpty) value.toDouble.toInt.toDouble else 0.0)
	formattedInput;
	}
///change string rdd to labeled point rdd for model training
def parseData(vals: Array[Double]): LabeledPoint = {
  LabeledPoint(if (vals(0)>=15) 1.0 else 0.0, Vectors.dense(vals.drop(1)))
}

}