import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark.rdd._
import scala.collection.JavaConverters._
import scala.collection.mutable.ListBuffer
import java.io._
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.mllib.feature.StandardScaler
import java.time.LocalDate 
import java.time.format.DateTimeFormatter
import org.apache.spark.mllib.classification.LogisticRegressionWithSGD
import org.apache.spark.mllib.tree.DecisionTree
import org.apache.spark.mllib.regression.LinearRegressionModel
import org.apache.spark.mllib.regression.LinearRegressionWithSGD
import org.apache.spark.mllib.util.MLUtils
import org.apache.spark.mllib.evaluation.BinaryClassificationMetrics
import org.apache.spark.mllib.tree.RandomForest
import org.apache.spark.mllib.tree.model.RandomForestModel
import org.apache.spark.mllib.evaluation.MulticlassMetrics
import org.apache.spark.mllib.classification.SVMWithSGD

object Intial_Models{

def main(args: Array[String]) {
	val conf = new SparkConf().setAppName("Flight Delay Detection")
	val sc = new SparkContext(conf)
	val data_1 = sc.textFile("hdfs://BDP/2015/jan_2015.csv,hdfs:///BDP/2015/feb_2015.csv,hdfs://2015/mar_2015.csv,hdfs://2015/apr_2015.csv,hdfs://2015/may_2015.csv,hdfs://2015/jun_2015.csv,hdfs://2015/jul_2015.csv,hdfs://2015/aug_2015.csv,hdfs://2015/sep_2015.csv,hdfs://2015/oct_2015.csv,hdfs://2015/nov_2015.csv,hdfs://2015/dec_2015.csv").filter(rec => rec.split(",")(17)=="0")
	//val dat1 = data_1.filter(rec=>rec.split(",")(7)=="LAX")
	val data1 = dat1.map(Intial_Models.preProcess)//map data to features.
	//data.take(5).map(x => x mkString ",").foreach(println)
	val data_2 = sc.textFile("hdfs://2016/apr_2016.csv,hdfs://2016/jan_2016.csv,hdfs://2016/feb_2016.csv,hdfs://2016/mar_2016.csv,hdfs://2016/may_2016.csv,hdfs://2016/jun_2016.csv,hdfs://2016/jul_16.csv,hdfs://2016/sep_16.csv,hdfs://2016/aug_2016.csv").filter(rec => rec.split(",")(17)=="0")
	val dat2 = data_2.filter(rec=>rec.split(",")(7)=="LAX")
	val data2 = dat2.map(Intial_Models.preProcess)//map data to features

// Prepare training set
	val parsedTrainData = data1.map(parseData)
	parsedTrainData.cache
	val scaler = new StandardScaler(withMean = true, withStd = true).fit(parsedTrainData.map(x => x.features))
	val scaledTrainData = parsedTrainData.map(x => LabeledPoint(x.label, scaler.transform(Vectors.dense(x.features.toArray))))
	scaledTrainData.cache

// Prepare test/validation set
	val parsedTestData = data2.map(parseData)
	parsedTestData.cache
	val scaledTestData = parsedTestData.map(x => LabeledPoint(x.label, scaler.transform(Vectors.dense(x.features.toArray))))
	scaledTestData.cache
	//scaledTrainData.take(3).map(x => (x.label, x.features)).foreach(println)
///////////////////// No delay model

	val labelsAndPreds_ND = parsedTestData.map { point =>(0.0, point.label)}
	val m_ND = eval_metrics(labelsAndPreds_ND)._2
	println("No Delay Model")
	println("precision = %.2f, recall = %.2f, F1 = %.2f, accuracy = %.2f".format(m_ND(0), m_ND(1), m_ND(2), m_ND(3)))	
	val cND = eval_metrics(labelsAndPreds_ND)._1
	println("TP = %.2f, TN = %.2f, FP = %.2f, FN = %.2f".format(cND(0), cND(1), cND(2), cND(3)))
	val metrics_ND = new BinaryClassificationMetrics(labelsAndPreds_ND)
	val auROC_ND = metrics_ND.areaUnderROC
	println("Area under ROC = " + auROC_ND)
	
///////////////////// Predict logistic regression
	val model_lr = LogisticRegressionWithSGD.train(scaledTrainData, numIterations=100)
	val labelsAndPreds_lr = scaledTestData.map { point =>
		val pred = model_lr.predict(point.features)
		(pred, point.label)
	}
	val m_lr = eval_metrics(labelsAndPreds_lr)._2
	println("Logistic Regression with SGD")
	println("precision = %.2f, recall = %.2f, F1 = %.2f, accuracy = %.2f".format(m_lr(0), m_lr(1), m_lr(2), m_lr(3)))
	val cLR = eval_metrics(labelsAndPreds_lr)._1
	println("TP = %.2f, TN = %.2f, FP = %.2f, FN = %.2f".format(cLR(0), cLR(1), cLR(2), cLR(3)))
	val metrics = new BinaryClassificationMetrics(labelsAndPreds_lr)
	val auROC = metrics.areaUnderROC
	println("Area under ROC = " + auROC)
	
//////////////////// Build the Decision Tree model
	val numClasses = 2
	val categoricalFeaturesInfo = Map[Int, Int]()
	val impurity = "gini"
	val maxDepth = 10
	val maxBins = 100
	val model_dt = DecisionTree.trainClassifier(parsedTrainData, numClasses, categoricalFeaturesInfo, impurity, maxDepth, maxBins)

	val labelsAndPreds_dt = parsedTestData.map { point =>
		val pred = model_dt.predict(point.features)
		(pred, point.label)		
	}
	val m_dt = eval_metrics(labelsAndPreds_dt)._2
	println("Decision Tree:")
	println("precision = %.2f, recall = %.2f, F1 = %.2f, accuracy = %.2f".format(m_dt(0), m_dt(1), m_dt(2), m_dt(3)))	
	val cM = eval_metrics(labelsAndPreds_dt)._1
	println("TP = %.2f, TN = %.2f, FP = %.2f, FN = %.2f".format(cM(0), cM(1), cM(2), cM(3)))
	val metrics_dt = new BinaryClassificationMetrics(labelsAndPreds_dt)
	val auROC_dt = metrics_dt.areaUnderROC
	println("Area under ROC = " + auROC_dt)

///////////////////////////////Random Forest	
	val model_rf = RandomForest.trainClassifier(parsedTrainData, 2, Map[Int, Int]((0,13),(1,32),(2,8),(3,25)),10, "auto", "gini", 10, 100)
// Evaluate model on test instances and compute test error
	
	val labelAndPreds_rf = parsedTestData.map { point =>
		val pre = model_rf.predict(point.features)
		(pre, point.label)
	}
	val metrics_rf = new BinaryClassificationMetrics(labelAndPreds_rf)
	val auROC_rf = metrics_rf.areaUnderROC
	println("Random Forest Model")
	println("Area under ROC = " + auROC_rf)
	val m_rf = eval_metrics(labelAndPreds_rf)._2
	println("precision = %.2f, recall = %.2f, F1 = %.2f, accuracy = %.2f".format(m_rf(0), m_rf(1), m_rf(2), m_rf(3)))	
	val crf = eval_metrics(labelAndPreds_rf)._1
	println("TP = %.2f, TN = %.2f, FP = %.2f, FN = %.2f".format(crf(0), crf(1), crf(2), crf(3)))
	
//////////////////multi class	// Build the Decision Tree model

	// Prepare training set
	val parsedTrainDataM = data1.map(parseDataM)
	parsedTrainDataM.cache
	val scalerM = new StandardScaler(withMean = true, withStd = true).fit(parsedTrainDataM.map(x => x.features))
	val scaledTrainDataM = parsedTrainDataM.map(x => LabeledPoint(x.label, scalerM.transform(Vectors.dense(x.features.toArray))))
	scaledTrainDataM.cache

// Prepare test/validation set
	val parsedTestDataM = data2.map(parseDataM)
	parsedTestDataM.cache
	val scaledTestDataM = parsedTestDataM.map(x => LabeledPoint(x.label, scalerM.transform(Vectors.dense(x.features.toArray))))
	scaledTestDataM.cache
// Predict
	val model_M = DecisionTree.trainClassifier(parsedTrainDataM, 5 ,Map[Int, Int]() ,"gini", 10, 100)
	val labelsAndPreds_M = parsedTestDataM.map { point =>
		val pred = model_M.predict(point.features)
		(pred, point.label)		
	}
	val metricsM = new MulticlassMetrics(labelsAndPreds_M)
	println("Multi Class Decision Tree")
	println("Confusion matrix:")
	println(metricsM.confusionMatrix)
	val labels = metricsM.labels
	labels.foreach { l =>
	println(s"Precision($l) = " + metricsM.precision(l))
	}
	// Recall by label
	labels.foreach { l =>
	println(s"Recall($l) = " + metricsM.recall(l))
	}
///////////////////////////////SVM with SGD
val svmAlg = new SVMWithSGD()
svmAlg.optimizer.setNumIterations(100)
                .setRegParam(1.0)
                .setStepSize(1.0)
val model_svm = svmAlg.run(scaledTrainData)

// Predict
val labelsAndPreds_svm = scaledTestData.map { point =>
        val pred = model_svm.predict(point.features)
        (pred, point.label)
	}
	val m_svm = eval_metrics(labelsAndPreds_svm)._2
	println("Linear SVM")
	println("precision = %.2f, recall = %.2f, F1 = %.2f, accuracy = %.2f".format(m_svm(0), m_svm(1), m_svm(2), m_svm(3)))
	val cSVM = eval_metrics(labelsAndPreds_svm)._1
	println("TP = %.2f, TN = %.2f, FP = %.2f, FN = %.2f".format(cSVM(0), cSVM(1), cSVM(2), cSVM(3)))
	val metrics_SVM = new BinaryClassificationMetrics(labelsAndPreds_svm)
	val auROC_SVM = metrics_SVM.areaUnderROC
	println("Area under ROC = " + auROC_SVM)

	
	parsedTrainData.unpersist()
	scaledTrainData.unpersist()
	parsedTestData.unpersist()
	scaledTestData.unpersist()
	parsedTrainDataM.unpersist()
	scaledTrainDataM.unpersist()
	parsedTestDataM.unpersist()
	scaledTestDataM.unpersist()

}

def eval_metrics(labelsAndPreds: RDD[(Double, Double)]) : Tuple2[Array[Double], Array[Double]] = {
    val tp = labelsAndPreds.filter(r => r._1==1 && r._2==1).count.toDouble
    val tn = labelsAndPreds.filter(r => r._1==0 && r._2==0).count.toDouble
    val fp = labelsAndPreds.filter(r => r._1==1 && r._2==0).count.toDouble
    val fn = labelsAndPreds.filter(r => r._1==0 && r._2==1).count.toDouble
	
	//val tp = labelsAndPreds.filter(r => r._1==0 && r._2==0).count.toDouble
	//println("true positives: ")
	//val tn = labelsAndPreds.filter(r => r._1==1 && r._2==1).count.toDouble
    //val fp = labelsAndPreds.filter(r => r._1==0 && r._2==1).count.toDouble
    //val fn = labelsAndPreds.filter(r => r._1==1 && r._2==0).count.toDouble
	
    val precision = tp / (tp+fp)
    val recall = tp / (tp+fn)
    val F_measure = 2*precision*recall / (precision+recall)
    val accuracy = (tp+tn) / (tp+tn+fp+fn)
    new Tuple2(Array(tp, tn, fp, fn), Array(precision, recall, F_measure, accuracy))
}

//depDelay,month,dayOfMonth,dayOfWeek,get_hour(crsDepTime),distance
//days_from_nearest_holiday(year.toInt, month.toInt, dayOfMonth.toInt)
///(rec(0),rec(1),rec(2),    rec(3),    rec(9),   rec(11),rec(7), rec(23),rec(17))
///year    month dayofmonth dayofweek crsdeptime depdelay origin distance cancelled

def preProcess1(data: String): Array[Double]={
val input = data.split(",")
val depDelay = input(11).toDouble
val month = input(1).toDouble
val dayOfMonth = input(2).toDouble
val dayOfWeek = input(3).toDouble
val hourofDay = "%04d".format(input(9).toInt).take(2).toDouble
val distance = input(23).toDouble
Array(depDelay,month,dayOfMonth,dayOfWeek,hourofDay,distance)
}

def preProcess(data: String): Array[Double]={
val input = data.split(",")
val year = input(0).toDouble
val depDelay = input(11).toDouble
val month = input(1).toDouble
val dayOfMonth = input(2).toDouble
val dayOfWeek = input(3).toDouble
val hourofDay = "%04d".format(input(9).toInt).take(2).toDouble
val distance = input(23).toDouble
val daysFromHoliday = daysFromHolidayFunc(year.toInt,month.toInt,dayOfMonth.toInt).toDouble
Array(depDelay,month,dayOfMonth,dayOfWeek,hourofDay,distance,daysFromHoliday)
}

val holidays = List("01/01/2015","01/19/2015","02/16/2015","05/25/2015","07/03/2016","09/07/2015","10/12/2015","11/11/2015","11/26/2015","12/25/2015","01/01/2016","01/18/2016","02/15/2016","05/30/2016","07/04/2016","09/05/2016")
def daysFromHolidayFunc(year:Int, month:Int, day:Int): Int = {
	  val currentDate = LocalDate.of(year,month,day)
      val formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy")
	  holidays.foldLeft(3000){ (res, cur) =>
		val holiday = LocalDate.parse(cur, formatter)
        val daysFromCur = Math.abs(holiday.toEpochDay()-currentDate.toEpochDay())
        math.min(res, daysFromCur.toInt)
      }
}

def parseData(vals: Array[Double]): LabeledPoint = {
  LabeledPoint(if (vals(0)>=15) 1.0 else 0.0, Vectors.dense(vals.drop(1)))
}

def parseDataM(vals: Array[Double]): LabeledPoint = {
  LabeledPoint(classNum(vals(0)), Vectors.dense(vals.drop(1)))
}
def classNum(delay: Double): Double={
	var clsNum = 0.0;
	if (delay<=0){ clsNum = 0.0;
	}else if (delay>0 && delay<=10){clsNum = 1.0;
	}else if (delay>10 && delay<=20){clsNum = 2.0;
	}else if (delay>20 && delay<=30){clsNum = 3.0;
	}else{clsNum = 4.0;
	}
	clsNum
}

}