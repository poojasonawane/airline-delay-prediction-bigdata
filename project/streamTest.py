from pyspark import SparkContext
from pyspark.streaming import StreamingContext
from pyspark.mllib.tree import DecisionTree, DecisionTreeModel
from pyspark.mllib.util import MLUtils


# Create a local StreamingContext with two working thread and batch interval of 1 second
sc.stop()
sc = SparkContext("local[2]", "StreamingtestData")
ssc = StreamingContext(sc, 1)

model = DecisionTreeModel.load(sc, "hdfs://Final_DT_Model")
# Create a DStream that will connect to hostname:port, like localhost:9999
testData = ssc.textFileStream("hdfs://test/data")
predictions = model.predict(testData.map(lambda x: x.features))
labelsAndPredictions = testData.map(lambda lp: lp.label).zip(predictions)
testMSE = labelsAndPredictions.map(lambda (v, p): (v - p) * (v - p)).sum() /\
    float(testData.count())
print('Test Mean Squared Error = ' + str(testMSE))
print('Learned regression tree model:')
print(model.toDebugString())

ssc.start()             # Start the computation
ssc.awaitTermination()  # Wait for the computation to terminate