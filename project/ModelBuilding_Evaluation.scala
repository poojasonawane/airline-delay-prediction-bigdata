import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark.rdd._
import scala.collection.JavaConverters._
import scala.collection.mutable.ListBuffer
import java.io._
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.mllib.feature.StandardScaler
import java.time.LocalDate 
import java.time.format.DateTimeFormatter
import org.apache.spark.mllib.classification.LogisticRegressionWithSGD
import org.apache.spark.mllib.tree.DecisionTree
import org.apache.spark.mllib.regression.LinearRegressionModel
import org.apache.spark.mllib.regression.LinearRegressionWithSGD
import org.apache.spark.mllib.util.MLUtils
import org.apache.spark.mllib.evaluation.BinaryClassificationMetrics
import org.apache.spark.mllib.tree.RandomForest
import org.apache.spark.mllib.tree.model.RandomForestModel
import org.apache.spark.mllib.evaluation.MulticlassMetrics
import org.apache.spark.mllib.classification.SVMWithSGD

object SimpleApp{

def main(args: Array[String]) {
	val conf = new SparkConf().setAppName("Simple Application")
	val sc = new SparkContext(conf)
	val data_1 = sc.textFile("hdfs:/data/categorset_train.csv").filter(_.nonEmpty)
	val data1 = data_1.map(SimpleApp.preProcess1)//map data to features.
	val data_2 = sc.textFile("hdfs:/data/categorset_test.csv").filter(_.nonEmpty)
	val data2 = data_2.map(SimpleApp.preProcess1)//map data to features
	//data2.take(5).map(x => x mkString ",").foreach(println)

// Prepare training set
	val parsedTrainData = data1.map(parseData)
	val scaler = new StandardScaler(withMean = true, withStd = true).fit(parsedTrainData.map(x => x.features))
	val scaledTrainData = parsedTrainData.map(x => LabeledPoint(x.label, scaler.transform(Vectors.dense(x.features.toArray))))
	
// Prepare test/validation set
	val parsedTestData = data2.map(parseData)
	
	val scaledTestData = parsedTestData.map(x => LabeledPoint(x.label, scaler.transform(Vectors.dense(x.features.toArray))))
	//scaledTrainData.take(3).map(x => (x.label, x.features)).foreach(println)
	
///////////////////// Predict logistic regression
	val model_lr = LogisticRegressionWithSGD.train(scaledTrainData, numIterations=100)
	val labelsAndPreds_lr = scaledTestData.map { point =>
		val pred = model_lr.predict(point.features)
		(pred, point.label)
	}
	val m_lr = eval_metrics(labelsAndPreds_lr)._2
	println("Logistic Regression with SGD")
	println("precision = %.2f, recall = %.2f, F1 = %.2f, accuracy = %.2f".format(m_lr(0), m_lr(1), m_lr(2), m_lr(3)))
	val cLR = eval_metrics(labelsAndPreds_lr)._1
	println("TP = %.2f, TN = %.2f, FP = %.2f, FN = %.2f".format(cLR(0), cLR(1), cLR(2), cLR(3)))
	val metrics = new BinaryClassificationMetrics(labelsAndPreds_lr)
	val auROC = metrics.areaUnderROC
	println("Area under ROC = " + auROC)
	
//////////////////// Build the Decision Tree model
	val numClasses = 2
	val categoricalFeaturesInfo = Map[Int, Int]())
	val impurity = "gini"
	val maxDepth = 10
	val maxBins = 100
	val model_dt = DecisionTree.trainClassifier(scaledTrainData, numClasses, categoricalFeaturesInfo, impurity, maxDepth, maxBins)

	val labelsAndPreds_dt = scaledTestData.map { point =>
		val pred = model_dt.predict(point.features)
		(pred, point.label)		
	}
	val m_dt = eval_metrics(labelsAndPreds_dt)._2
	println("Decision Tree:")
	println("precision = %.2f, recall = %.2f, F1 = %.2f, accuracy = %.2f".format(m_dt(0), m_dt(1), m_dt(2), m_dt(3)))	
	val cM = eval_metrics(labelsAndPreds_dt)._1
	println("TP = %.2f, TN = %.2f, FP = %.2f, FN = %.2f".format(cM(0), cM(1), cM(2), cM(3)))
	val metrics_dt = new BinaryClassificationMetrics(labelsAndPreds_dt)
	val auROC_dt = metrics_dt.areaUnderROC
	println("Area under ROC = " + auROC_dt)
	
}

def eval_metrics(labelsAndPreds: RDD[(Double, Double)]) : Tuple2[Array[Double], Array[Double]] = {
    val tp = labelsAndPreds.filter(r => r._1==1 && r._2==1).count.toDouble
    val tn = labelsAndPreds.filter(r => r._1==0 && r._2==0).count.toDouble
    val fp = labelsAndPreds.filter(r => r._1==1 && r._2==0).count.toDouble
    val fn = labelsAndPreds.filter(r => r._1==0 && r._2==1).count.toDouble
    val precision = tp / (tp+fp)
    val recall = tp / (tp+fn)
    val F_measure = 2*precision*recall / (precision+recall)
    val accuracy = (tp+tn) / (tp+tn+fp+fn)
    new Tuple2(Array(tp, tn, fp, fn), Array(precision, recall, F_measure, accuracy))
}

//depDelay,month,dayOfMonth,dayOfWeek,get_hour(crsDepTime),distance
//days_from_nearest_holiday(year.toInt, month.toInt, dayOfMonth.toInt)
///(rec(0),rec(1),rec(2),    rec(3),    rec(9),   rec(11),rec(7), rec(23),rec(17))
///year    month dayofmonth dayofweek crsdeptime depdelay origin distance cancelled

def preProcess1(data: String): Array[Double]={
	val input = data.split(",").dropRight(1);
	//println(input(0))
	val formattedInput = input.map(value => if (!value.isEmpty) value.toDouble.toInt.toDouble else 0.0)
	formattedInput;
	}


def parseData(vals: Array[Double]): LabeledPoint = {
  LabeledPoint(if (vals(0)>=15) 1.0 else 0.0, Vectors.dense(vals.drop(1)))
}


}