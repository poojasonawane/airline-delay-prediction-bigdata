import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark.rdd._
import scala.collection.JavaConverters._
import scala.collection.mutable.ListBuffer
import java.io._
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.mllib.feature.StandardScaler
import java.time.LocalDate 
import java.time.format.DateTimeFormatter
import org.apache.spark.mllib.classification.LogisticRegressionWithSGD
import org.apache.spark.mllib.tree.DecisionTree
import org.apache.spark.mllib.regression.LinearRegressionModel
import org.apache.spark.mllib.regression.LinearRegressionWithSGD
import org.apache.spark.mllib.util.MLUtils
import org.apache.spark.mllib.evaluation.BinaryClassificationMetrics
import org.apache.spark.mllib.tree.RandomForest
import org.apache.spark.mllib.tree.model.RandomForestModel
import org.apache.spark.mllib.evaluation.MulticlassMetrics
import org.apache.spark.mllib.classification.SVMWithSGD

object Train&SaveFinalDecisionTree_Model{

def main(args: Array[String]) {
	val conf = new SparkConf().setAppName("Flight Delay Detection")
	val sc = new SparkContext(conf)
	val data_train = sc.textFile("hdfs://train/categorset_train.csv").filter(_.nonEmpty)
	val train = data_train.map(Train&SaveFinalDecisionTree_Model.preProcess)//map data to features.

//////////////////// Prepare training set
	val parsedTrainData = train.map(parseData)
	val scaler = new StandardScaler(withMean = true, withStd = true).fit(parsedTrainData.map(x => x.features))
	val scaledTrainData = parsedTrainData.map(x => LabeledPoint(x.label, scaler.transform(Vectors.dense(x.features.toArray))))
	
//////////////////// Build the Decision Tree model
	val numClasses = 2
	val categoricalFeaturesInfo = Map[Int, Int]()
	val impurity = "gini"
	val maxDepth = 50
	val maxBins = 100
	
	val model_dt = DecisionTree.trainClassifier(scaledTrainData, numClasses, categoricalFeaturesInfo, impurity, maxDepth, maxBins)

	model_dt.save(sc, "hdfs://target/tmp/Final_DT_Model")
}
////preprocess data.
def preProcess(data: String): Array[Double]={
	val input = data.split(",").dropRight(1);
	val formattedInput = input.map(value => if (!value.isEmpty) value.toDouble.toInt.toDouble else 0.0)
	formattedInput;
	}
///change string rdd to labeled point rdd for model training
def parseData(vals: Array[Double]): LabeledPoint = {
  LabeledPoint(if (vals(0)>=15) 1.0 else 0.0, Vectors.dense(vals.drop(1)))
}

}